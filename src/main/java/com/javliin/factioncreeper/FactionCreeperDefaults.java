package com.javliin.factioncreeper;

import java.util.ArrayList;
import java.util.List;

public class FactionCreeperDefaults {

    private List<String> blocks;

    private boolean ally;
    private boolean enemy;
    private boolean truce;
    private boolean neutral;

    public FactionCreeperDefaults(List<String> blocks, Boolean ally, Boolean enemy, Boolean truce, Boolean neutral) {
        this.blocks = blocks;

        this.ally = ally;
        this.enemy = enemy;
        this.truce = truce;
        this.neutral = neutral;
    }

    public void setBlocks(List<String> blocks) { this.blocks = blocks; }

    public void setAlly(Boolean ally) { this.ally = ally; }
    public void setEnemy(Boolean enemy) { this.enemy = enemy; }
    public void setTruce(Boolean truce) { this.truce = truce; }
    public void setNeutral(Boolean neutral) { this.neutral = neutral; }

    public List<String> getBlocks() { return new ArrayList<String>() {{ this.addAll(blocks); }}; }

    public Boolean getAlly() { return ally; }
    public Boolean getEnemy() { return enemy; }
    public Boolean getTruce() { return truce; }
    public Boolean getNeutral() { return neutral; }
}
