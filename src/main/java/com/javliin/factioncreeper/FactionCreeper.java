package com.javliin.factioncreeper;

import com.massivecraft.factions.Rel;
import com.massivecraft.factions.entity.BoardColl;
import com.massivecraft.factions.entity.Faction;
import com.massivecraft.factions.entity.FactionColl;
import com.massivecraft.factions.entity.MPlayer;
import com.massivecraft.factions.Event.EventFactionsCreate;
import com.massivecraft.factions.Event.EventFactionsDisband;
import com.massivecraft.factions.Event.EventFactionsNameChange;
import com.massivecraft.massivecore.ps.PS;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.Event.EventHandler;
import org.bukkit.Event.Listener;
import org.bukkit.Event.block.Action;
import org.bukkit.Event.entity.CreatureSpawnEvent;
import org.bukkit.Event.entity.EntityExplodeEvent;
import org.bukkit.Event.player.PlayerInteractEvent;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class FactionCreeper extends JavaPlugin implements Listener, CommandExecutor {

    private FactionCreeperDefaults defaults;

    private HashMap<Integer, Player> creeperSpawns;
    private HashMap<Double[], Player> attemptedSpawn;

    public void onEnable() {
        this.creeperSpawns = new HashMap<>();
        this.attemptedSpawn = new HashMap<>();

        this.saveDefaultConfig();

        this.defaults = new FactionCreeperDefaults(getConfig().getStringList("default.blockList"), getConfig().getBoolean("default.ALLY"), getConfig().getBoolean("default.ENEMY"),
                getConfig().getBoolean("default.TRUCE"), getConfig().getBoolean("default.NEUTRAL"));

        int factions = 0;

        for(Faction faction : FactionColl.get().getAll())
            if(getConfig().get(faction.getName()) == null) {
                if(!faction.isNormal() || faction.getName().equals("SafeZone") || faction.getName().equals("WarZone"))
                    continue;

                addNewFaction(faction.getName());
                factions++;
            }

        if(factions > 0)
            getLogger().info(factions + " unregistered faction(s) found! Saving...");

        saveConfig();
        getServer().getPluginManager().registerEvents(this, this);
        getCommand("factioncreeper").setExecutor(this);
    }

    public void onDisable() {
        saveConfig();
    }

    private boolean isWhitelistEnabled(Rel relation, String factionName) {
        return (getConfig().get(factionName + "." + relation.toString()) == true);
    }

    private boolean isMaterialInConfig(Material material, String factionName) {
        return (getConfig().getStringList(factionName + ".blockList").contains(material.name()));
    }

    private void addNewFaction(String factionName) {
        getConfig().set(factionName + ".ALLY", defaults.getAlly());
        getConfig().set(factionName + ".ENEMY", defaults.getEnemy());
        getConfig().set(factionName + ".TRUCE", defaults.getTruce());
        getConfig().set(factionName + ".NEUTRAL", defaults.getNeutral());
        getConfig().set(factionName + ".blockList", defaults.getBlocks());
        saveConfig();
    }

    private void removeBadBlocks(List<Block> blocks, Faction placerFaction) {
        Iterator<Block> iterator = blocks.iterator();
        while(iterator.hasNext()) {
            Block current = iterator.next();
            if(isWhitelistEnabled(placerFaction.getRelationTo(BoardColl.get().getFactionAt(PS.valueOf(current.getLocation()))),
                    BoardColl.get().getFactionAt(PS.valueOf(current.getLocation())).getName()) &&
                    isMaterialInConfig(current.getType(), BoardColl.get().getFactionAt(PS.valueOf(current.getLocation())).getName())) {
                System.out.println(current.getType().name());
                continue;
            }
            iterator.remove();
        }
    }

    @EventHandler
    public void onFactionCreate(EventFactionsCreate e) {
        addNewFaction(e.getFactionName());
    }

    @EventHandler
    public void onFactionRename(EventFactionsNameChange e) {
        getConfig().set(e.getNewName() + ".blockList", getConfig().get(e.getFaction().getName() + ".blockList"));
        getConfig().set(e.getNewName() + ".ALLY",  getConfig().get(e.getFaction().getName() + ".ALLY"));
        getConfig().set(e.getNewName() + ".ENEMY",  getConfig().get(e.getFaction().getName() + ".ENEMY"));
        getConfig().set(e.getNewName() + ".TRUCE",  getConfig().get(e.getFaction().getName() + ".TRUCE"));
        getConfig().set(e.getNewName() + ".NEUTRAL",  getConfig().get(e.getFaction().getName() + ".NEUTRAL"));
        getConfig().set(e.getFaction().getName(), null);
        saveConfig();
    }

    @EventHandler
    public void onFactionDisband(EventFactionsDisband e) {
        getConfig().set(e.getFaction().getName(), null);
    }

    @EventHandler
    public void onRightClick(PlayerInteractEvent e) {
        if(e.getAction() != Action.RIGHT_CLICK_BLOCK)
            return;

        if(e.getPlayer().getItemInHand().getType() != Material.MONSTER_EGG)
            return;

        final Location location = e.getClickedBlock().getLocation();

        for(Double[] location_ : attemptedSpawn.keySet())
            if(location_[0] == location.getX() + .5 && location_[1] == location.getY() + 1 && location_[2] == location.getZ() + .5) {
                e.setCancelled(true);
            }

        attemptedSpawn.put(new Double[]{location.getX() + .5, location.getY() + 1, location.getZ() + .5}, e.getPlayer());

        getServer().getScheduler().scheduleSyncDelayedTask(this, new Runnable() {
            @Override
            public void run() {
                for(Double[] location_ : attemptedSpawn.keySet())
                    if(location_[0] == location.getX() + .5 && location_[1] == location.getY() + 1 && location_[2] == location.getZ() + .5) {
                        attemptedSpawn.remove(location_);
                    }
            }
        }, 5L);
    }

    @EventHandler
    public void onCreatureSpawn(CreatureSpawnEvent e) {
        if(e.getSpawnReason() != CreatureSpawnEvent.SpawnReason.SPAWNER_EGG)
            return;

        for(Double[] location : attemptedSpawn.keySet())
            if(location[0] == e.getLocation().getX() && location[1] == e.getLocation().getY() && location[2] == e.getLocation().getZ()) {
                creeperSpawns.put(e.getEntity().getEntityId(),attemptedSpawn.get(location));
            }
    }

    @EventHandler
    public void onCreeperExplode(EntityExplodeEvent e) {
        if(creeperSpawns.get(e.getEntity().getEntityId()) == null)
            return;

        if(!BoardColl.get().getFactionAt(PS.valueOf(e.getLocation())).isNormal() || BoardColl.get().getFactionAt(PS.valueOf(e.getLocation())).getName().equals("SafeZone")
                || BoardColl.get().getFactionAt(PS.valueOf(e.getLocation())).getName().equals("WarZone"))
            return;

        if(MPlayer.get(creeperSpawns.get(e.getEntity().getEntityId())).getFaction().getName().equals(BoardColl.get().getFactionAt(PS.valueOf(e.getLocation())).getName()))
            return;

        removeBadBlocks(e.blockList(), MPlayer.get(creeperSpawns.get(e.getEntity().getEntityId())).getFaction());
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if(!(sender.hasPermission("factioncreeper.load") || sender.hasPermission("factioncreeper.setdefaults"))) {
            sender.sendMessage(ChatColor.RED + "No permission.");
            return true;
        }

        if(args.length < 1) {
            sender.sendMessage(ChatColor.RED + "Missing arguments.");
            return true;
        }

        if(args[0].equalsIgnoreCase("load") && sender.hasPermission("factioncreeper.load")) {
            reloadConfig();
            sender.sendMessage(ChatColor.RED + "Operation performed.");
            return true;
        }

        if(args[0].equalsIgnoreCase("setdefaults") && sender.hasPermission("factioncreeper.setdefaults"))
            for(String s : getConfig().getKeys(false)) {
                if(s.equals("default"))
                    continue;

                addNewFaction(s);
                sender.sendMessage(ChatColor.RED + "Operation performed.");
                return true;
            }

        sender.sendMessage(ChatColor.RED + "No permission.");
        return true;
    }
}
